<?php

namespace Database\Seeders;

use App\Models\PriceManagement;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeed::class);
        $this->call(AdminSeed::class);
        $this->call(VendorSeed::class);
    }
}
