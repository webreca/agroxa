@extends('layouts.admin')
@section('title','Dashboard')
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Welcome to Dashboard</li>
                </ol>
            </div>
        </div>
    </div>

    <div class="page-content-wrapper">
        <div class="row">
            <div class="col-xl-3 col-md-6">
                <a href="{{route('admin.customers.index')}}">
                    <div class="card bg-primary mini-stat position-relative">
                        <div class="card-body">
                            <div class="mini-stat-desc">
                                <div class="text-white">
                                    <h6 class="text-uppercase mt-0 text-white-50">Customers</h6>
                                    <h3 class="mb-3 mt-0">{{ $total_customers }}</h3>
                                </div>
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-cube-outline display-3"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-3 col-md-6">
                <a href="{{route('admin.my-account.edit', Auth::guard('admin')->id())}}">
                    <div class="card bg-primary mini-stat position-relative">
                        <div class="card-body">
                            <div class="mini-stat-desc">
                                <div class="text-white">
                                    <h6 class="text-uppercase mt-0 text-white-50">My Account</h6>
                                </div>
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-buffer display-3"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-3 col-md-6">
                <a href="{{route('admin.password.form')}}">
                    <div class="card bg-primary mini-stat position-relative">
                        <div class="card-body">
                            <div class="mini-stat-desc">
                                <div class="text-white">
                                    <h6 class="text-uppercase mt-0 text-white-50">Change Password</h6>
                                </div>
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-tag-text-outline display-3"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-3 col-md-6">
                <a href="{{ route('logout') }}"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <div class="card bg-primary mini-stat position-relative">
                        <div class="card-body">
                            <div class="mini-stat-desc">
                                <div class="text-white">
                                    <h6 class="text-uppercase mt-0 text-white-50">Logout</h6>
                                </div>
                                <div class="mini-stat-icon">
                                    <i class="mdi mdi-briefcase-check display-3"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
