@extends('layouts.admin')
@section('title','Edit Customer')
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Edit Customer</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashhboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">User Management</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.customers.index') }}">Customers</a></li>
                    <li class="breadcrumb-item">Edit</li>
                </ol>

                <div class="state-information d-none d-sm-block">
                    <a href="{{ url()->previous() }}" class="btn btn-danger waves-effect">Back</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="page-content-wrapper">
        <div class="row">
            <div class="col-md-12 mb-4">
                <div class="card m-b-20">
                    <div class="card-body">
                        <form action="{{ route('admin.customers.update', $customer->id) }}" method="POST" id="customerForm">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <label for="firstname" class="col-sm-2 col-form-label">Firstname</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="firstname" placeholder="Firstname"
                                        name="firstname" value="{{ old('firstname', $customer->firstname) }}">
                                    @error('firstname')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lastname" class="col-sm-2 col-form-label">Lastname</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="lastname" placeholder="Lastname"
                                        name="lastname"  value="{{ old('lastname', $customer->lastname) }}">
                                    @error('lastname')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-sm-2 col-form-label">Email Address</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="email" placeholder="Email Address"
                                        name="email"  value="{{ old('email', $customer->email) }}">
                                    @error('email')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phone" class="col-sm-2 col-form-label">Phone Number</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="phone" placeholder="Phone Number"
                                        name="phone"  value="{{ old('phone', $customer->phone) }}">
                                    @error('phone')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="statuses" class="col-sm-2 col-form-label">Active</label>
                                <div class="col-sm-10">
                                        <input type="checkbox" id="statuses" name="status" value="active" @if(old('status', $customer->status) == 'active') checked @endif switch="primary">
                                        <label for="statuses" data-on-label="Yes" data-off-label="No"></label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-xl-12 col-md-12 text-right">
                                    <button class="btn btn-success" type="submit"
                                        form="customerForm">Update</button>
                                </div>
                            </div>

                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page content-->

</div> <!-- container-fluid -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" action="" id="deleteForm">
                @csrf
                <input type="hidden" name="_method" value="DELETE">
                <div class="modal-header bg-warning">
                    <h4 class="modal-title has-icon text-white"><i class="flaticon-alert-1"></i> Are you sure ?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body">
                    <p>You won't be able to revert this customer once deleted!</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary shadow-none">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')


@endpush
