@extends('layouts.admin')
@section('head')
<link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('title','Customers')
@section('content')
<div class="container-fluid">

    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-box">
                <h4 class="page-title">Customers</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashhboard</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0);">User Management</a></li>
                    <li class="breadcrumb-item active">Customers</li>
                </ol>

                <div class="state-information d-none d-sm-block">
                    <a href="javascript:void(0)" id="filter" class="btn btn-warning waves-effect">Filter</a>
                    <a href="{{ route('admin.customers.create') }}" class="btn btn-danger waves-effect">Create</a>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="page-content-wrapper">
        <div class="row">
            <div class="col-12 mb-4" id="filterBox">
                <div class="card">
                        <div class="card-header">
                            <h6>Filter</h6>
                        </div>
                    <div class="card-body">
                        <form action="{{ route('admin.customers.index') }}">
                            <div class="form-row">
                                <div class="col-xl-3 col-md-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="name" placeholder="Customer"
                                            name="name" value="{{ $filter_name }}">
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-12">
                                    <div class="input-group">
                                        <input type="email" class="form-control" id="email" placeholder="Email Address"
                                            name="email" value="{{ $filter_email }}">
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-12">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="phone" placeholder="Phone number"
                                            name="phone" value="{{ $filter_phone }}">
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-12">
                                    <div class="input-group">
                                        <select class="form-control" id="statuses" name="status">
                                            <option value="">Select Status</option>
                                            <option value="active" @if($filter_status=='active' ) selected @endif>Active
                                            </option>
                                            <option value="inactive" @if($filter_status=='inactive' ) selected @endif>
                                                Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xl-2 col-md-12 text-right">
                                    <button class="btn btn-info" type="submit"><i
                                            class="fas fa-filter"></i></button>
                                    <a href="{{ route('admin.customers.index') }}"
                                        class="btn btn-dark" type="submit"><i
                                            class="fas fa-undo"></i></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12">
                            @if(count($customers) > 0)
                            <div class="table-responsive">
                                <table id="datatable-buttons"
                                    class="table table-striped table-bordered dt-responsive nowrap"
                                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th scope="col">Id</th>
                                            <th scope="col">Customer</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Phone</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($customers as $customer)
                                        <tr>
                                            <th scope="row">{{ $customer->id }}</th>
                                            <td>{{ $customer->firstname }} {{ $customer->lastname }}</td>
                                            <td>{{ $customer->email }}</td>
                                            <td>{{ $customer->phone }}</td>
                                            @if($customer->status == 'inactive')
                                            <td><span class="badge badge-danger">{{ ucfirst($customer->status)
                                                    }}</span>
                                            </td>
                                            @else
                                            <td><span class="badge badge-success">{{ ucfirst($customer->status)
                                                    }}</span>
                                            </td>
                                            @endif
                                            <td>
                                                <a class="btn btn-dark" href="{{ route('admin.customers.edit', $customer->id) }}"><i
                                                        class="fas fa-pencil-alt ms-text-success"></i> </a>
                                                <a class="btn btn-danger" href="#modal-delete" data-id="{{ $customer->id }}"
                                                    class="confirmDelete" data-toggle="modal"
                                                    data-target="#modal-delete"><i
                                                        class="far fa-trash-alt ms-text-danger"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <div class="table-responsive">
                                <p class="text-center">No Listing found.</p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page content-->

</div> <!-- container-fluid -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-delete">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="POST" action="" id="deleteForm">
                @csrf
              <input type="hidden" name="_method" value="DELETE">
                <div class="modal-header bg-warning">
                    <h4 class="modal-title has-icon text-white"><i class="flaticon-alert-1"></i> Are you sure ?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body">
                    <p>You won't be able to revert this customer once deleted!</p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary shadow-none">Confirm</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js')}}"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js')}}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js')}}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js')}}"></script>
<!-- Confirm Delete Scripts Start -->
<script>
    $(document).on("click", ".confirmDelete", function (e) {

    e.preventDefault();

    var _self = $(this);

    var requestId = _self.data('id');

    form_action = '{{ route("admin.customers.destroy", ":id") }}';
    url = form_action.replace(':id', requestId);

    $('#deleteForm').attr('action', url);


    $(_self.attr('href')).modal('show');
    });
</script>
<!-- Confirm Delete Scripts End -->

<!-- Filter Box Scripts Start -->
<script>
    $(document).ready(function(){
        var filterBox = '{{ $filter_box }}';
        if(filterBox === 'show'){
            $("#filterBox").css('display', 'block');
        }

        $("#filter").click(function(){
            $("#filterBox").slideToggle();
        });

    });
</script>
<!-- Filter Box Scripts End -->

@endpush
