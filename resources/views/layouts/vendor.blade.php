<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>@yield('title')</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Themesbrand" name="author" />
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">
    @yield('head')
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/metismenu.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="wrapper">
        @include('vendor.sections.header')
        @include('vendor.sections.sidebar-left')
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                @include('vendor.sections.flashmessage')
                @yield('content')
            </div>
            <!-- End content -->
        </div>
    </div>
    <!-- SCRIPTS -->
    <!-- jQuery  -->
    <script src="{{ asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{ asset('assets/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('assets/js/metisMenu.min.js')}}"></script>
    <script src="{{ asset('assets/js/jquery.slimscroll.js')}}"></script>
    <script src="{{ asset('assets/js/waves.min.js')}}"></script>

    <script src="{{ asset('plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- Peity JS -->
    <script src="{{ asset('plugins/peity/jquery.peity.min.js')}}"></script>

    <script src="{{ asset('plugins/morris/morris.min.js')}}"></script>
    <script src="{{ asset('plugins/raphael/raphael-min.js')}}"></script>

    <script src="{{ asset('assets/pages/dashboard.js')}}"></script>
    @stack('scripts')

    <!-- App js -->
    <script src="{{ asset('assets/js/app.js')}}"></script>

</body>

</html>
