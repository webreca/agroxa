<?php

namespace App\Http\Controllers\Vendor\Auth;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest:vendor')->except('logout');
    }

    public function showLoginForm()
    {
        return view('vendor.auth.login');
    }

    public function login(Request $request)
    {
        // Validate form data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ]);

        $vendor = Vendor::where($this->username(), '=', $request->input($this->username()))->first();

        if ($vendor && $vendor->status == 'inactive') {
            throw ValidationException::withMessages([$this->username() => __('The account is Inactive. Please Contact Administrator')]);
        }

        // Attempt to log the user in
        if(Auth::guard('vendor')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
            return redirect()->intended(route('vendor.dashboard'));

        }else{

         return $this->sendFailedLoginResponse($request);
     }

 }

    public function logout(Request $request)
    {

        Auth::guard('vendor')->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route( 'vendor.login' );
    }
}
