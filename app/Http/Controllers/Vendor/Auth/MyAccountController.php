<?php

namespace App\Http\Controllers\Vendor\Auth;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class MyAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:vendor');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Vendor $vendor)
    {
        $id = Auth::guard('vendor')->id();
        $vendor = Vendor::find($id);
        $vendor->avatar = isset($vendor->avatar) ? asset('storage/uploads/vendor/'.$vendor->avatar) : URL::to('assets/images/placeholder.png') ;
        return view('vendor.auth.my-account', compact('vendor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'firstname'=>'required',
            'email'=>'required',
        ]);

        $vendor = Vendor::find($id);
        $vendor->firstname = $request->firstname;
        $vendor->lastname = $request->lastname;
        $vendor->email = $request->email;
        $vendor->phone = $request->phone;

        if($request->hasfile('avatar')){

            $image = $request->file('avatar');

            $name = $image->getClientOriginalName();

            $image->storeAs('uploads/vendor/', $name, 'public');

            if(isset($vendor->avatar)){

                $path = 'public/uploads/vendor/'.$vendor->avatar;

                Storage::delete($path);

            }

            $vendor->avatar = $name;

        }

        $vendor->save();

        return redirect()->route('vendor.my-account.edit', $vendor->id)->with('success', 'Account updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendor $vendor)
    {
        //
    }
}
