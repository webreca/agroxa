<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter_box     = 'hide';
        $filter_name    = $request->name;
        $filter_email   = $request->email;
        $filter_phone   = $request->phone;
        $filter_status  = $request->status;

        if(isset($filter_name) || isset($filter_email) || isset($filter_phone) || isset($filter_status)){
            $filter_box = 'show';
        }
        $customers = User::orderBy('id', 'desc');

        if(isset($filter_name)){
            $customers =  $customers->where(DB::raw("concat(firstname, ' ', lastname)"), 'LIKE', "%".$filter_name."%");
        }
        if(isset($filter_email)){
            $customers =  $customers->whereEmail($filter_email);
        }
        if(isset($filter_phone)){
            $customers =  $customers->where('phone', 'LIKE', "%".$filter_phone."%");
        }
        if(isset($filter_status)){
            $customers =  $customers->where('status', $filter_status);
        }

        $customers = $customers->get();

        return view('admin.customers.list', compact('customers', 'filter_box', 'filter_name', 'filter_email', 'filter_phone', 'filter_status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $customer               = new User;
        $customer->firstname    = $request->firstname;
        $customer->lastname     = $request->lastname;
        $customer->email        = $request->email;
        $customer->phone        = $request->phone;
        $customer->password     = Hash::make($request->password);
        $customer->status       = isset($request->status) ? 'active' : 'inactive';
        $customer->save();

        return redirect()->route('admin.customers.index')->with('success', 'Customer created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = User::find($id);
        return view('admin.customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'firstname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $id],
        ]);

        $customer               = User::find($id);
        $customer->firstname    = $request->firstname;
        $customer->lastname     = $request->lastname;
        $customer->email        = $request->email;
        $customer->phone        = $request->phone;
        $customer->status       = isset($request->status) ? 'active' : 'inactive';
        $customer->save();

        return redirect()->route('admin.customers.index')->with('success', 'Customer updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user     = User::find($id)->delete();
        return redirect()->route('admin.customers.index')->with('success', 'Customer deleted successfully.');
    }
}
