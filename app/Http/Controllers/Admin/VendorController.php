<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class VendorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter_box     = 'hide';
        $filter_name    = $request->name;
        $filter_email   = $request->email;
        $filter_phone   = $request->phone;
        $filter_status  = $request->status;

        if(isset($filter_name) || isset($filter_email) || isset($filter_phone) || isset($filter_status)){
            $filter_box = 'show';
        }
        $vendors = Vendor::orderBy('id', 'desc');

        if(isset($filter_name)){
            $vendors =  $vendors->where(DB::raw("concat(firstname, ' ', lastname)"), 'LIKE', "%".$filter_name."%");
        }
        if(isset($filter_email)){
            $vendors =  $vendors->whereEmail($filter_email);
        }
        if(isset($filter_phone)){
            $vendors =  $vendors->where('phone', 'LIKE', "%".$filter_phone."%");
        }
        if(isset($filter_status)){
            $vendors =  $vendors->where('status', $filter_status);
        }

        $vendors = $vendors->paginate(20);

        return view('admin.vendors.list', compact('vendors', 'filter_box', 'filter_name', 'filter_email', 'filter_phone', 'filter_status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.vendors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:admins'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        $vendor               = new Vendor;
        $vendor->firstname    = $request->firstname;
        $vendor->lastname     = $request->lastname;
        $vendor->email        = $request->email;
        $vendor->phone        = $request->phone;
        $vendor->password     = Hash::make($request->password);
        $vendor->status       = isset($request->status) ? 'active' : 'inactive';
        $vendor->save();

        return redirect()->route('admin.vendors.index')->with('success', 'Vendor created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vendor = Vendor::find($id);
        return view('admin.vendors.edit', compact('vendor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'firstname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $id],
        ]);

        $vendor               = Vendor::find($id);
        $vendor->firstname    = $request->firstname;
        $vendor->lastname     = $request->lastname;
        $vendor->email        = $request->email;
        $vendor->phone        = $request->phone;
        $vendor->status       = isset($request->status) ? 'active' : 'inactive';
        $vendor->save();

        return redirect()->route('admin.vendors.index')->with('success', 'Vendor updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user     = Vendor::find($id)->delete();
        return redirect()->route('admin.vendors.index')->with('success', 'Vendor deleted successfully.');
    }
}
