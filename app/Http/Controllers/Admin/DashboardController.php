<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;

class DashboardController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $total_customers = User::count();
        $customers = User::latest()->take(5)->get();
        return view('admin.dashboard', compact('total_customers', 'customers'));
    }
}
